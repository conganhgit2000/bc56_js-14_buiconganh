function ketQua() {
    var soThu1 = document.getElementById("so-thu-1").value * 1;
    var soThu2 = document.getElementById("so-thu-2").value * 1;
    var soThu3 = document.getElementById("so-thu-3").value * 1;

    var soLe = 0;
    var soChan = 0;

    if (soThu1 % 2 === 0) {
        soChan++;
    } else {
        soLe++;
    }

    if (soThu2 % 2 === 0) {
        soChan++;
    } else {
        soLe++;
    }

    if (soThu3 % 2 === 0) {
        soChan++;
    } else {
        soLe++;
    }

    document.getElementById("result").innerHTML = `<h2 class= "mt-3 alert-warning">${"Số lẻ: " + soLe + "<br>Số chẵn: " + soChan}</h2>`;
}