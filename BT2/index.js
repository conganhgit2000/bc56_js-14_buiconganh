function chaoHoi() {
    var giaDinh = document.getElementById("gia-dinh").value;

    switch (giaDinh) {
        case "B": {
            giaDinh = "Xin chào Bố";
            break;
        }
        case "M": {
            giaDinh = "Xin chào Mẹ";
            break;
        }
        case "A": {
            giaDinh = "Xin chào Anh trai";
            break;
        }
        case "E": {
            giaDinh = "Xin chào Em gái";
            break;
        }
    }

    document.getElementById("result").innerHTML = `<h2 class="mt-3 alert-warning">${giaDinh}</h2>`;
}