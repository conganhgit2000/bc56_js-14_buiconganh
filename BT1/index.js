function sapXep() {
    var num1 = document.getElementById("so-thu-1").value * 1;
    var num2 = document.getElementById("so-thu-2").value * 1;
    var num3 = document.getElementById("so-thu-3").value * 1;

    // Sắp xếp các số
    var soThu1, soThu2, soThu3;

    if (num1 <= num2 && num1 <= num3) {
        soThu1 = num1;
        if (num2 <= num3) {
            soThu2 = num2;
            soThu3 = num3;
        } else {
            soThu2 = num3;
            soThu3 = num2;
        }
    } else if (num2 <= num1 && num2 <= num3) {
        soThu1 = num2;
        if (num1 <= num3) {
            soThu2 = num1;
            soThu3 = num3;
        } else {
            soThu2 = num3;
            soThu3 = num1;
        }
    } else {
        soThu1 = num3;
        if (num1 <= num2) {
            soThu2 = num1;
            soThu3 = num2;
        } else {
            soThu2 = num2;
            soThu3 = num1;
        }
    }

    document.getElementById("result").innerHTML = `<h3 class="mt-4 alert-success"> ${soThu1 + ", " + soThu2 + ", " + soThu3 } </h3>`
}